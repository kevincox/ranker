# Ranker

A very simple tool to turn rankings from a group of people into normalized scores.

**Note:** This is fairly hardcoded for the problem I had. It isn't widely useful in its current form and there is a decent chance that I won't ever touch it again. However feel free to send patches if you want I wouldn't mind making it somewhat reusable.

For now it really just removes bias from the rankings. So if one person always ranks between 7 and 10 and someone else ranks between 4 and 7 they will have the same effect on the aggregate scores. This is done independently for each category.
