struct Vote {
	shop: &'static str,
	category: &'static str,
	person: &'static str,
	score: f32,
}

struct Review {
	avg: f32,
	min: f32,
	max: f32,
	count: u8,
}

fn main() {
	let votes = &[
		Vote{shop: "Bakerbots", category: "Overall", person: "Carter", score: 8.0},
		Vote{shop: "Bakerbots", category: "Overall", person: "Elaine", score: 7.0},
		Vote{shop: "Bakerbots", category: "Overall", person: "Kevin", score: 7.0},
		Vote{shop: "Bakerbots", category: "Overall", person: "Sam", score: 8.0},
		Vote{shop: "Bakerbots", category: "Ice Cream", person: "Carter", score: 8.0},
		Vote{shop: "Bakerbots", category: "Ice Cream", person: "Elaine", score: 7.0},
		Vote{shop: "Bakerbots", category: "Ice Cream", person: "Kevin", score: 4.0},
		Vote{shop: "Bakerbots", category: "Ice Cream", person: "Sam", score: 7.0},
		Vote{shop: "Bakerbots", category: "Variety", person: "Kevin", score: 7.0},
		Vote{shop: "Bakerbots", category: "Variety", person: "Sam", score: 9.0},
		Vote{shop: "Bakerbots", category: "Uniqueness", person: "Carter", score: 7.0},
		Vote{shop: "Bakerbots", category: "Uniqueness", person: "Elaine", score: 6.0},
		Vote{shop: "Bakerbots", category: "Uniqueness", person: "Kevin", score: 4.0},
		Vote{shop: "Bakerbots", category: "Uniqueness", person: "Sam", score: 8.0},
		Vote{shop: "Knockout", category: "Overall", person: "Carter", score: 7.0},
		Vote{shop: "Knockout", category: "Overall", person: "Elaine", score: 8.0},
		Vote{shop: "Knockout", category: "Overall", person: "Kevin", score: 7.0},
		Vote{shop: "Knockout", category: "Overall", person: "Sam", score: 7.0},
		Vote{shop: "Knockout", category: "Ice Cream", person: "Carter", score: 7.0},
		Vote{shop: "Knockout", category: "Ice Cream", person: "Elaine", score: 9.0},
		Vote{shop: "Knockout", category: "Ice Cream", person: "Kevin", score: 7.0},
		Vote{shop: "Knockout", category: "Ice Cream", person: "Sam", score: 6.0},
		Vote{shop: "Knockout", category: "Variety", person: "Elaine", score: 7.0},
		Vote{shop: "Knockout", category: "Variety", person: "Kevin", score: 7.0},
		Vote{shop: "Knockout", category: "Variety", person: "Sam", score: 9.0},
		Vote{shop: "Knockout", category: "Uniqueness", person: "Carter", score: 6.0},
		Vote{shop: "Knockout", category: "Uniqueness", person: "Elaine", score: 6.0},
		Vote{shop: "Knockout", category: "Uniqueness", person: "Kevin", score: 5.0},
		Vote{shop: "Knockout", category: "Uniqueness", person: "Sam", score: 6.0},
		Vote{shop: "Nani's", category: "Overall", person: "Carter", score: 9.0},
		Vote{shop: "Nani's", category: "Overall", person: "Elaine", score: 8.0},
		Vote{shop: "Nani's", category: "Overall", person: "Kevin", score: 8.0},
		Vote{shop: "Nani's", category: "Overall", person: "Sam", score: 8.0},
		Vote{shop: "Nani's", category: "Ice Cream", person: "Carter", score: 6.0},
		Vote{shop: "Nani's", category: "Ice Cream", person: "Elaine", score: 8.0},
		Vote{shop: "Nani's", category: "Ice Cream", person: "Kevin", score: 9.0},
		Vote{shop: "Nani's", category: "Ice Cream", person: "Sam", score: 8.0},
		Vote{shop: "Nani's", category: "Variety", person: "Elaine", score: 8.0},
		Vote{shop: "Nani's", category: "Variety", person: "Kevin", score: 5.0},
		Vote{shop: "Nani's", category: "Variety", person: "Sam", score: 5.0},
		Vote{shop: "Nani's", category: "Uniqueness", person: "Carter", score: 9.0},
		Vote{shop: "Nani's", category: "Uniqueness", person: "Elaine", score: 9.0},
		Vote{shop: "Nani's", category: "Uniqueness", person: "Kevin", score: 9.0},
		Vote{shop: "Nani's", category: "Uniqueness", person: "Sam", score: 9.0},
		Vote{shop: "Wong's", category: "Overall", person: "Elaine", score: 8.0},
		Vote{shop: "Wong's", category: "Overall", person: "Kevin", score: 7.0},
		Vote{shop: "Wong's", category: "Ice Cream", person: "Elaine", score: 8.0},
		Vote{shop: "Wong's", category: "Ice Cream", person: "Kevin", score: 7.0},
		Vote{shop: "Wong's", category: "Variety", person: "Elaine", score: 9.0},
		Vote{shop: "Wong's", category: "Variety", person: "Kevin", score: 6.0},
		Vote{shop: "Wong's", category: "Uniqueness", person: "Elaine", score: 10.0},
		Vote{shop: "Wong's", category: "Uniqueness", person: "Kevin", score: 7.0},
		Vote{shop: "Kekou", category: "Overall", person: "Elaine", score: 9.0},
		Vote{shop: "Kekou", category: "Overall", person: "Kevin", score: 8.0},
		Vote{shop: "Kekou", category: "Ice Cream", person: "Elaine", score: 10.0},
		Vote{shop: "Kekou", category: "Ice Cream", person: "Kevin", score: 8.0},
		Vote{shop: "Kekou", category: "Variety", person: "Elaine", score: 10.0},
		Vote{shop: "Kekou", category: "Variety", person: "Kevin", score: 6.0},
		Vote{shop: "Kekou", category: "Uniqueness", person: "Elaine", score: 9.0},
		Vote{shop: "Kekou", category: "Uniqueness", person: "Kevin", score: 7.0},
		Vote{shop: "Nadege", category: "Overall", person: "Elaine", score: 7.0},
		Vote{shop: "Nadege", category: "Overall", person: "Kevin", score: 8.0},
		Vote{shop: "Nadege", category: "Ice Cream", person: "Elaine", score: 8.0},
		Vote{shop: "Nadege", category: "Ice Cream", person: "Kevin", score: 7.0},
		Vote{shop: "Nadege", category: "Variety", person: "Elaine", score: 6.0},
		Vote{shop: "Nadege", category: "Variety", person: "Kevin", score: 7.0},
		Vote{shop: "Nadege", category: "Uniqueness", person: "Elaine", score: 6.0},
		Vote{shop: "Nadege", category: "Uniqueness", person: "Kevin", score: 5.0},
		Vote{shop: "Solado", category: "Overall", person: "Elaine", score: 8.0},
		Vote{shop: "Solado", category: "Overall", person: "Kevin", score: 8.0},
		Vote{shop: "Solado", category: "Ice Cream", person: "Elaine", score: 10.0},
		Vote{shop: "Solado", category: "Ice Cream", person: "Kevin", score: 7.0},
		Vote{shop: "Solado", category: "Variety", person: "Elaine", score: 6.0},
		Vote{shop: "Solado", category: "Variety", person: "Kevin", score: 4.0},
		Vote{shop: "Solado", category: "Uniqueness", person: "Elaine", score: 6.0},
		Vote{shop: "Solado", category: "Uniqueness", person: "Kevin", score: 8.0},
	];

	let mut average_rating_by_person_by_category = std::collections::HashMap::new();
	for vote in votes {
		let (ref mut total, ref mut count) = average_rating_by_person_by_category
			.entry((vote.person, vote.category))
			.or_insert((0.0, 0));
		*total += vote.score;
		*count += 1;
	}
	for (_, (ref mut total, count)) in &mut average_rating_by_person_by_category {
		*total /= *count as f32;
	}

	let mut results = std::collections::BTreeMap::new();
	for vote in votes {
		let r = results
			.entry((vote.shop, vote.category))
			.or_insert(Review {
				avg: 0.0,
				min: f32::INFINITY,
				max: f32::NEG_INFINITY,
				count: 0,
			});
		let normalized = vote.score - average_rating_by_person_by_category[&(vote.person, vote.category)].0;
		r.avg += normalized;
		r.min = r.min.min(normalized);
		r.max = r.max.max(normalized);
		r.count += 1;
	}
	println!("shop\tcategory\tavg\tmin\tmax\tcount");
	for ((shop, category), r) in &mut results {
		r.avg /= r.count as f32;

		println!("{}\t{}\t{}\t{}\t{}\t{}", shop, category, r.avg, r.min, r.max, r.count);
	}
}
